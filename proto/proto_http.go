// +build http

package proto

import "net/http"

type SpeechServer struct {
	Addr string
	Channel chan string
	httpServ *http.Server
}

func (s *SpeechServer) Proto() string {
	return "http"
}

func (s *SpeechServer) Init() {
	s.httpServ = &http.Server{ Addr: s.Addr }
	smux := http.NewServeMux()

	smux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
		s.Channel<- r.FormValue("s")
		w.WriteHeader(204)
	})
	s.httpServ.Handler = smux
}

func (s *SpeechServer) ListenAndServe() {
	s.httpServ.ListenAndServe()
}

