// +build osc

package proto

import "fmt"
import "github.com/hypebeast/go-osc/osc"

type SpeechServer struct {
	Addr string
	Channel chan string
	oscServ *osc.Server
}

func (s *SpeechServer) Proto() string {
	return "osc"
}

func (s *SpeechServer) Init() {
	s.oscServ = &osc.Server{ Addr: s.Addr }
	s.oscServ.Handle("/text", func(msg *osc.Message) {
		switch msg.Arguments[0].(type) {
		case string:
			s.Channel<- msg.Arguments[0].(string)
		default:
			fmt.Println("debug: not a string...")
		}
	})
}

func (s *SpeechServer) ListenAndServe() {
	s.oscServ.ListenAndServe();
}

