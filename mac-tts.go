package main

import "fmt"
import "os"
import "os/exec"
import "strings"

import "bitbucket.org/chtisgit/mac-tts/proto"

const DEFAULT_ADDR = ":7890"

func speechDispatcher(text <-chan string) {
	for {
		str := <-text
		str = strings.Map(func(x rune) rune {
			if x == '\n' || x == '\r' || x == '\x00' {
				return -1
			}
			return x
		}, str)
		fmt.Println("- ", str)
		cmd := exec.Command("say", str)
		cmd.Run()
	}
}

func main() {
	interf := DEFAULT_ADDR
	if len(os.Args) >= 2 {
		interf = os.Args[1]
	}

	ch := make(chan string, 50)
	server := &proto.SpeechServer{Addr: interf, Channel: ch}
	server.Init()

	go speechDispatcher(ch)

	fmt.Printf("Listening on %s://%s. Ready.\n", server.Proto(), interf)
	server.ListenAndServe()
}
